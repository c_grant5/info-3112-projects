﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace info3112_projects.Controllers
{
    [Route("/active-sprint")]
    public class ActiveSprintController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}