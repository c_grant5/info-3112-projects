﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using info3112_projects.Models;

namespace info3112_projects.Controllers
{
    public class StoryController : Controller
    {
        private readonly StoryModel storyMod;
        AppDbContext _db;

        public StoryController(AppDbContext ctx)
        {
            _db = ctx;
            storyMod = new StoryModel(_db);
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("/user-story/")]
        public IActionResult CreateProjectInfo([FromBody] Story story)
        {
            try
            {
                storyMod.AddStory(story);
                return Ok(storyMod);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("/user-story/{id}/")]
        public IActionResult GetStory(int id)
        {
            try
            {
                Story story = storyMod.GetStory(id);
                return Ok(story);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [Route("/user-story/")]
        public IActionResult UpdateStory([FromBody] Story story)
        {
            try
            {
                storyMod.UpdateStory(story);
                return Ok(story);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [Route("/user-story/{id}")]
        public IActionResult InputHours([FromBody] int actHours, int id)
        {
            try
            {
                storyMod.ActualHours(actHours, id);
                return Ok(storyMod);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
