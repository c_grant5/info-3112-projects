﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using info3112_projects.Models;
using Microsoft.AspNetCore.Mvc;

namespace info3112_projects.Controllers
{

    [Route("/team")]
    public class TeamController : Controller
    {
        private readonly TeamMemberModel teamMod;

        public TeamController(AppDbContext context)
        {
            teamMod = new TeamMemberModel(context);
        }

        [HttpGet]
        public IActionResult GetTeamMembers()
        {
            try
            {
                TeamMember[] teamMembers = teamMod.getAllMembers();
                return Ok(teamMembers);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}