﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using info3112_projects.Models;
using Task = System.Threading.Tasks.Task;

namespace info3112_projects.Controllers
{
    public class ProjectInfoController : Controller
    {
        private readonly ProjectModel projMod;

        public ProjectInfoController(AppDbContext context)
        {
            projMod = new ProjectModel(context);
        }

        [HttpPost]
        [Route("/project-info")]
        public IActionResult CreateProjectInfo([FromBody] Project projectModel)
        {
            try
            {
                Console.WriteLine(projectModel);
                projMod.AddPM(projectModel);
                return Ok(projectModel);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}