﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using info3112_projects.Models;
using Microsoft.AspNetCore.Mvc;
using Task = info3112_projects.Models.Task;

namespace info3112_projects.Controllers
{
    [Route("/backlog")]
    public class BacklogController : Controller
    {
        AppDbContext _db;

        public BacklogController(AppDbContext ctx)
        {
            _db = ctx;
        }

        [HttpGet]
        [Route("/backlog")]
        public IActionResult GetAllBacklogs()
        {
            try
            {
                BacklogModel bm = new BacklogModel(_db);
                return Ok(bm.GetAll());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}