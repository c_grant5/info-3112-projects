import {UserStory} from './user-story.model';

export class Backlog {
  stories: UserStory[];
}
