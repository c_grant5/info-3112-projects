import {Task} from './task.model';
import {Status} from './status.model';

export class UserStory {
  story_ID: number;
  description: string;
  estimate: number;
  actual: number;
  status: Status;
  tasks: Task[];
}
