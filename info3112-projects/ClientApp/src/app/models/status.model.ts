export enum Status {
  Planned = 1,
  Started,
  Delivered
}

export namespace Status {
  export function values() {
    return Object.keys(Status).filter(
      (type) => isNaN(<any>type) && type !== 'values'
    );
  }
}
