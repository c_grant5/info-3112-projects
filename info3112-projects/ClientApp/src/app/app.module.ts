import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';

import {AppComponent} from './app.component';
import {MaterialModule} from 'app/modules/material.module';
import {NavMenuComponent} from './nav-menu/nav-menu.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {CounterComponent} from './counter/counter.component';
import {FetchDataComponent} from './fetch-data/fetch-data.component';
import {ActiveSprintComponent} from './active-sprint/active-sprint.component';
import {TeamComponent} from './team/team.component';
import {ReportsComponent} from './reports/reports.component';
import {BacklogComponent} from './backlog/backlog.component';
import {ProjectInfoComponent} from './project-info/project-info.component';
import {UserStoryComponent} from './user-story/user-story.component';
import { AddStoryComponent } from './user-story/add-story/add-story.component';

import {ProjectInfoService} from './services/project-info.service';
import {TeamService} from './services/team.service';
import {BacklogService} from './services/backlog.service';
import {StoryService} from './services/story.service';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    DashboardComponent,
    CounterComponent,
    FetchDataComponent,
    ActiveSprintComponent,
    TeamComponent,
    ReportsComponent,
    BacklogComponent,
    ProjectInfoComponent,
    UserStoryComponent,
    AddStoryComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'ng-cli-universal'}),
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {path: '', component: DashboardComponent, pathMatch: 'full'},
      {path: 'active-sprint', component: ActiveSprintComponent},
      {path: 'team', component: TeamComponent},
      {path: 'reports', component: ReportsComponent},
      {path: 'backlog', component: BacklogComponent},
      {path: 'project-info', component: ProjectInfoComponent},
      {path: 'user-story/:id', component: UserStoryComponent}
    ])
  ],
  exports: [MaterialModule],
  providers: [ProjectInfoService, TeamService, BacklogService, StoryService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule {
}
