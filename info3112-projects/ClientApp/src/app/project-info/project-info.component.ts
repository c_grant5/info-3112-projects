import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ProjectInfoService} from '../services/project-info.service';

@Component({
  selector: 'app-project-info',
  templateUrl: './project-info.component.html',
  styleUrls: ['./project-info.component.css']
})
export class ProjectInfoComponent implements OnInit {

  projectInfoForm: FormGroup;

  constructor(private fb: FormBuilder, private projectInfoService: ProjectInfoService) {
  }

  ngOnInit() {
    this.projectInfoForm = this.fb.group({
      team_name: ['', Validators.required],
      product_name: ['', Validators.required],
      hyperlink: ['', Validators.required],
      start_date: ['', Validators.required],
      point_hour_ratio: ['', Validators.required],
      velocity: ['', Validators.required]
    });
  }

  onSubmit() {
    // TODO: implement call to route
    this.projectInfoService.createProjectInfo(this.projectInfoForm.value)
      .subscribe(response => console.log(response));
  }
}
