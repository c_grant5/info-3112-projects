import { Component, OnInit } from '@angular/core';
import {UserStory} from '../models/user-story.model';
import {BacklogService} from '../services/backlog.service';

@Component({
  selector: 'app-backlog',
  templateUrl: './backlog.component.html',
  styleUrls: ['./backlog.component.css']
})
export class BacklogComponent implements OnInit {

  stories: UserStory[];
  isStoriesLoaded = false;
  constructor(private backlogService: BacklogService) { }

  ngOnInit() {
    this.backlogService.getAllStories().subscribe((backlog: UserStory[]) => {
      this.stories = backlog;
      this.isStoriesLoaded = true;
    });
  }

}
