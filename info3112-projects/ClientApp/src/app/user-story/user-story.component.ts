import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {StoryService} from '../services/story.service';
import {UserStory} from '../models/user-story.model';
import {Status} from '../models/status.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-user-story',
  templateUrl: './user-story.component.html',
  styleUrls: ['./user-story.component.css']
})
export class UserStoryComponent implements OnInit {

  story: UserStory;
  userStoryForm: FormGroup;
  statuses: string[];
  statusSelected = '';

  constructor(
    private fb: FormBuilder,
    private storyService: StoryService,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.statuses = Status.values();

    this.userStoryForm = this.fb.group({
      description: ['', Validators.required],
      estimate: [''],
      status: ['', Validators.required]
    });

    this.storyService.getStory(this.route.snapshot.paramMap.get('id'))
      .subscribe((story: UserStory) => {
        this.story = story;
        this.userStoryForm.patchValue(story);
        this.statusSelected = this.statuses[story.status];
    });
  }

  onSaveStory() {
    
  }
}
