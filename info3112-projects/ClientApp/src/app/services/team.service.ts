import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/Observable';
import {TeamMember} from '../models/team-member.model';

@Injectable()
export class TeamService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) {
  }

  getAllTeamMembers(): Observable<TeamMember[]> {
    return this.http.get<TeamMember[]>('/team', this.httpOptions);
  }
}
