import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Backlog} from '../models/backlog.model';
import {UserStory} from '../models/user-story.model';

@Injectable()
export class BacklogService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) { }

  getAllStories(): Observable<UserStory[]> {
    return this.http.get<UserStory[]>('/backlog', this.httpOptions);
  }
}
