import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {UserStory} from '../models/user-story.model';

@Injectable()
export class StoryService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) { }

  getStory(id: string): Observable<UserStory> {
    return this.http.get<UserStory>(`/user-story/${id}`, this.httpOptions);
  }
}
