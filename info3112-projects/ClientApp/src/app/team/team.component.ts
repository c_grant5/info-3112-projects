import {Component, OnInit} from '@angular/core';
import {TeamMember} from '../models/team-member.model';
import {TeamService} from '../services/team.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {

  teamMembers: TeamMember[];
  columnHeaders: string[];

  constructor(private teamService: TeamService) {
  }

  ngOnInit() {
    this.columnHeaders = ['name', 'accuracy'];
    this.teamService.getAllTeamMembers().subscribe((members: TeamMember[]) =>
      this.teamMembers = members);
  }
}
