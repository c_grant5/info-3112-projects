﻿using info3112_projects.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace info3112_projects
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }
        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<Sprint> Sprints { get; set; }
        public virtual DbSet<Story> Stories { get; set; }
        public virtual DbSet<Models.Task> Tasks { get; set; }
        public virtual DbSet<TeamMember> TeamMembers { get; set; }
        public virtual DbSet<Backlog> Backlogs { get; set; }
        public virtual DbSet<MemberTask> MemberTasks { get; set; }
    }
}
