﻿
-- TEAM MEMBER DATA
DELETE FROM TeamMembers;

INSERT INTO TeamMembers (Name, Accuracy) VALUES ('John', 77);
INSERT INTO TeamMembers (Name, Accuracy) VALUES ('Jim', 69);
INSERT INTO TeamMembers (Name, Accuracy) VALUES ('Jane', 75);
INSERT INTO TeamMembers (Name, Accuracy) VALUES ('Jeff', 43);
INSERT INTO TeamMembers (Name, Accuracy) VALUES ('Carl', 98);
INSERT INTO TeamMembers (Name, Accuracy) VALUES ('Chris', 55);
INSERT INTO TeamMembers (Name, Accuracy) VALUES ('George', 88);
INSERT INTO TeamMembers (Name, Accuracy) VALUES ('Bob', 22);

SELECT * FROM TeamMembers;

-- PROJECT DATA

DELETE FROM Projects;

INSERT INTO Projects(Point_Hour_Ratio, Team_Name, Product_Name, Hyperlink, Start_Date, Velocity)
VALUES (2, 'Team1', 'Product1', 'www.google.com', getdate(),3);
INSERT INTO Projects(Point_Hour_Ratio, Team_Name, Product_Name, Hyperlink, Start_Date, Velocity)
VALUES (4, 'Team2', 'Product2', 'www.google.com', getdate(), 4);
INSERT INTO Projects(Point_Hour_Ratio, Team_Name, Product_Name, Hyperlink, Start_Date, Velocity)
VALUES (2, 'Team3', 'Product3', 'www.google.com', getdate(),3);
INSERT INTO Projects(Point_Hour_Ratio, Team_Name, Product_Name, Hyperlink, Start_Date, Velocity)
VALUES (4, 'Team4', 'Product4', 'www.google.com', getdate(), 4);
INSERT INTO Projects(Point_Hour_Ratio, Team_Name, Product_Name, Hyperlink, Start_Date, Velocity)
VALUES (2, 'Team5', 'Product5', 'www.google.com', getdate(),3);
INSERT INTO Projects(Point_Hour_Ratio, Team_Name, Product_Name, Hyperlink, Start_Date, Velocity)
VALUES (2, 'Team6', 'Product6', 'www.google.com', getdate(),3);
INSERT INTO Projects(Point_Hour_Ratio, Team_Name, Product_Name, Hyperlink, Start_Date, Velocity)
VALUES (4, 'Team7', 'Product7', 'www.google.com', getdate(), 4);
INSERT INTO Projects(Point_Hour_Ratio, Team_Name, Product_Name, Hyperlink, Start_Date, Velocity)
VALUES (2, 'Team8', 'Product8', 'www.google.com', getdate(),3);
INSERT INTO Projects(Point_Hour_Ratio, Team_Name, Product_Name, Hyperlink, Start_Date, Velocity)
VALUES (4, 'Team9', 'Product9', 'www.google.com', getdate(), 4);
INSERT INTO Projects(Point_Hour_Ratio, Team_Name, Product_Name, Hyperlink, Start_Date, Velocity)
VALUES (2, 'Team10', 'Product10', 'www.google.com', getdate(),3);
INSERT INTO Projects(Point_Hour_Ratio, Team_Name, Product_Name, Hyperlink, Start_Date, Velocity)
VALUES (4, 'Team11', 'Product11', 'www.google.com', getdate(), 4);
INSERT INTO Projects(Point_Hour_Ratio, Team_Name, Product_Name, Hyperlink, Start_Date, Velocity)
VALUES (2, 'Team12', 'Product12', 'www.google.com', getdate(),3);
INSERT INTO Projects(Point_Hour_Ratio, Team_Name, Product_Name, Hyperlink, Start_Date, Velocity)
VALUES (4, 'Team13', 'Product13', 'www.google.com', getdate(), 4);
INSERT INTO Projects(Point_Hour_Ratio, Team_Name, Product_Name, Hyperlink, Start_Date, Velocity)
VALUES (2, 'Team14', 'Product14', 'www.google.com', getdate(),3);
INSERT INTO Projects(Point_Hour_Ratio, Team_Name, Product_Name, Hyperlink, Start_Date, Velocity)
VALUES (4, 'Team15', 'Product15', 'www.google.com', getdate(), 4);
INSERT INTO Projects(Point_Hour_Ratio, Team_Name, Product_Name, Hyperlink, Start_Date, Velocity)
VALUES (4, 'Team16', 'Product16', 'www.google.com', getdate(), 4);

SELECT * FROM projects;

-- Sprint