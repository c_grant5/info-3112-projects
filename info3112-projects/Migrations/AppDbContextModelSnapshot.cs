﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using info3112_projects;

namespace info3112projects.Migrations
{
    [DbContext(typeof(AppDbContext))]
    partial class AppDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.3-servicing-35854")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("info3112_projects.Models.Backlog", b =>
                {
                    b.Property<int>("Backlog_ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<double>("Hours_Estimated");

                    b.Property<int>("Project_ID");

                    b.HasKey("Backlog_ID");

                    b.HasIndex("Project_ID");

                    b.ToTable("Backlogs");
                });

            modelBuilder.Entity("info3112_projects.Models.MemberTask", b =>
                {
                    b.Property<int>("Member_Task_ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<double>("Hours_Worked");

                    b.Property<int>("Story_ID");

                    b.HasKey("Member_Task_ID");

                    b.HasIndex("Story_ID");

                    b.ToTable("MemberTasks");
                });

            modelBuilder.Entity("info3112_projects.Models.Project", b =>
                {
                    b.Property<int>("Project_ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Hyperlink");

                    b.Property<int>("Point_Hour_Ratio");

                    b.Property<string>("Product_Name");

                    b.Property<DateTime?>("Start_Date");

                    b.Property<string>("Team_Name");

                    b.Property<int>("Velocity");

                    b.HasKey("Project_ID");

                    b.ToTable("Projects");
                });

            modelBuilder.Entity("info3112_projects.Models.Sprint", b =>
                {
                    b.Property<int>("Sprint_ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("End_Date");

                    b.Property<double>("Hours_Estimated");

                    b.Property<int>("Project_ID");

                    b.Property<DateTime>("Start_Date");

                    b.HasKey("Sprint_ID");

                    b.HasIndex("Project_ID");

                    b.ToTable("Sprints");
                });

            modelBuilder.Entity("info3112_projects.Models.Story", b =>
                {
                    b.Property<int>("Story_ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<double>("Actual");

                    b.Property<int?>("Backlog_ID");

                    b.Property<string>("Description");

                    b.Property<double>("Estimate");

                    b.Property<int?>("Sprint_ID");

                    b.Property<int>("Status");

                    b.HasKey("Story_ID");

                    b.HasIndex("Backlog_ID");

                    b.HasIndex("Sprint_ID");

                    b.ToTable("Stories");
                });

            modelBuilder.Entity("info3112_projects.Models.Task", b =>
                {
                    b.Property<int>("Task_ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<bool>("IsCompleted");

                    b.Property<string>("Name");

                    b.Property<int>("Story_ID");

                    b.HasKey("Task_ID");

                    b.HasIndex("Story_ID");

                    b.ToTable("Tasks");
                });

            modelBuilder.Entity("info3112_projects.Models.TeamMember", b =>
                {
                    b.Property<int>("Team_Member_ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<double>("Accuracy");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.HasKey("Team_Member_ID");

                    b.ToTable("TeamMembers");
                });

            modelBuilder.Entity("info3112_projects.Models.Backlog", b =>
                {
                    b.HasOne("info3112_projects.Models.Project", "Project")
                        .WithMany()
                        .HasForeignKey("Project_ID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("info3112_projects.Models.MemberTask", b =>
                {
                    b.HasOne("info3112_projects.Models.Story", "Story")
                        .WithMany()
                        .HasForeignKey("Story_ID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("info3112_projects.Models.Sprint", b =>
                {
                    b.HasOne("info3112_projects.Models.Project", "Project")
                        .WithMany()
                        .HasForeignKey("Project_ID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("info3112_projects.Models.Story", b =>
                {
                    b.HasOne("info3112_projects.Models.Backlog", "Backlog")
                        .WithMany("Stories")
                        .HasForeignKey("Backlog_ID");

                    b.HasOne("info3112_projects.Models.Sprint", "Sprint")
                        .WithMany("Stories")
                        .HasForeignKey("Sprint_ID");
                });

            modelBuilder.Entity("info3112_projects.Models.Task", b =>
                {
                    b.HasOne("info3112_projects.Models.Story", "Story")
                        .WithMany("StoryTasks")
                        .HasForeignKey("Story_ID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
