﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace info3112projects.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Project_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Point_Hour_Ratio = table.Column<int>(nullable: false),
                    Team_Name = table.Column<string>(nullable: true),
                    Product_Name = table.Column<string>(nullable: true),
                    Hyperlink = table.Column<string>(nullable: true),
                    Start_Date = table.Column<DateTime>(nullable: true),
                    Velocity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Project_ID);
                });

            migrationBuilder.CreateTable(
                name: "TeamMembers",
                columns: table => new
                {
                    Team_Member_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Accuracy = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeamMembers", x => x.Team_Member_ID);
                });

            migrationBuilder.CreateTable(
                name: "Backlogs",
                columns: table => new
                {
                    Backlog_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Project_ID = table.Column<int>(nullable: false),
                    Hours_Estimated = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Backlogs", x => x.Backlog_ID);
                    table.ForeignKey(
                        name: "FK_Backlogs_Projects_Project_ID",
                        column: x => x.Project_ID,
                        principalTable: "Projects",
                        principalColumn: "Project_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Sprints",
                columns: table => new
                {
                    Sprint_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Project_ID = table.Column<int>(nullable: false),
                    Hours_Estimated = table.Column<double>(nullable: false),
                    Start_Date = table.Column<DateTime>(nullable: false),
                    End_Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sprints", x => x.Sprint_ID);
                    table.ForeignKey(
                        name: "FK_Sprints_Projects_Project_ID",
                        column: x => x.Project_ID,
                        principalTable: "Projects",
                        principalColumn: "Project_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Stories",
                columns: table => new
                {
                    Story_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Sprint_ID = table.Column<int>(nullable: true),
                    Backlog_ID = table.Column<int>(nullable: true),
                    Estimate = table.Column<double>(nullable: false),
                    Actual = table.Column<double>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stories", x => x.Story_ID);
                    table.ForeignKey(
                        name: "FK_Stories_Backlogs_Backlog_ID",
                        column: x => x.Backlog_ID,
                        principalTable: "Backlogs",
                        principalColumn: "Backlog_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Stories_Sprints_Sprint_ID",
                        column: x => x.Sprint_ID,
                        principalTable: "Sprints",
                        principalColumn: "Sprint_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MemberTasks",
                columns: table => new
                {
                    Member_Task_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Story_ID = table.Column<int>(nullable: false),
                    Hours_Worked = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MemberTasks", x => x.Member_Task_ID);
                    table.ForeignKey(
                        name: "FK_MemberTasks_Stories_Story_ID",
                        column: x => x.Story_ID,
                        principalTable: "Stories",
                        principalColumn: "Story_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    Task_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Story_ID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    IsCompleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.Task_ID);
                    table.ForeignKey(
                        name: "FK_Tasks_Stories_Story_ID",
                        column: x => x.Story_ID,
                        principalTable: "Stories",
                        principalColumn: "Story_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Backlogs_Project_ID",
                table: "Backlogs",
                column: "Project_ID");

            migrationBuilder.CreateIndex(
                name: "IX_MemberTasks_Story_ID",
                table: "MemberTasks",
                column: "Story_ID");

            migrationBuilder.CreateIndex(
                name: "IX_Sprints_Project_ID",
                table: "Sprints",
                column: "Project_ID");

            migrationBuilder.CreateIndex(
                name: "IX_Stories_Backlog_ID",
                table: "Stories",
                column: "Backlog_ID");

            migrationBuilder.CreateIndex(
                name: "IX_Stories_Sprint_ID",
                table: "Stories",
                column: "Sprint_ID");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_Story_ID",
                table: "Tasks",
                column: "Story_ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MemberTasks");

            migrationBuilder.DropTable(
                name: "Tasks");

            migrationBuilder.DropTable(
                name: "TeamMembers");

            migrationBuilder.DropTable(
                name: "Stories");

            migrationBuilder.DropTable(
                name: "Backlogs");

            migrationBuilder.DropTable(
                name: "Sprints");

            migrationBuilder.DropTable(
                name: "Projects");
        }
    }
}
