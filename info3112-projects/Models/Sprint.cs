﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace info3112_projects.Models
{
    public class Sprint
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Sprint_ID { get; set; }
        public int Project_ID { get; set; }
        public double Hours_Estimated { get; set; }
        public DateTime Start_Date { get; set; }
        public DateTime End_Date { get; set; }
        [ForeignKey("Project_ID")]
        public Project Project { get; set; }
        public ICollection<Story> Stories { get; set; }
    }
}
