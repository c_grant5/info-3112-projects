﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace info3112_projects.Models
{
    public enum Status
{
    Planned = 1,
    Started = 2,
    Delivered = 3
}
}
