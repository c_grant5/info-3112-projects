﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace info3112_projects.Models
{
    public class ProjectModel
    {
        private AppDbContext _db;
        public ProjectModel(AppDbContext ctx)
        {
            _db = ctx;
        }

        public List<Project> GetAll()
        {
            return _db.Projects.ToList();
        }

        public void AddPM(Project proj)
        {
            try
            {
                using (_db)
                {
                    using (var _trans = _db.Database.BeginTransaction())
                    {
                        try
                        {
                            _db.Projects.Add(proj);
                            _db.SaveChanges();
                            _trans.Commit();
                        }
                        catch(Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            _trans.Rollback();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
