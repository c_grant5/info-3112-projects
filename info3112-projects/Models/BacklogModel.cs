﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace info3112_projects.Models
{
    public class BacklogModel
    {
        private AppDbContext _db;
        public BacklogModel(AppDbContext ctx)
        {
            _db = ctx;
        }

        public List<Story> GetAll()
        {
            TaskModel tm = new TaskModel(_db);
            var temp = _db.Stories.Where(s => s.Backlog_ID != null).ToList();
            foreach(Story s in temp)
            {
                s.StoryTasks = tm.GetAllTasksFromStory(s.Story_ID);
            }
            return temp;
        }
    }
}
