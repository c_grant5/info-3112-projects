﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace info3112_projects.Models
{
    public class SprintModel
    {
        private AppDbContext _db;
        public SprintModel(AppDbContext ctx)
        {
            _db = ctx;
        }

        public List<Sprint> GetAllSprints()
        {
            return _db.Sprints.ToList();
        }

        public List<Sprint> GetAllSprintsFromProject(int id)
        {
            return _db.Sprints.Where(sprint => sprint.Project_ID == id).ToList();
        }

        public void AddSprint(Sprint sprint)
        {
            try
            {
                using (_db)
                {
                    using (var _trans = _db.Database.BeginTransaction())
                    {
                        try
                        {
                            _db.Sprints.Add(sprint);
                            _db.SaveChanges();
                            _trans.Commit();
                        }
                        catch(Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            _trans.Rollback();
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
