﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace info3112_projects.Models
{
    public class Story
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Story_ID { get; set; }
        public int? Sprint_ID { get; set; }
        public int? Backlog_ID { get; set; }
        public double Estimate { get; set; }
        public double Actual { get; set; }
        public string Description { get; set; }
        public Status Status { get; set; }
        [ForeignKey("Sprint_ID")]
        public Sprint Sprint { get; set; }
        [ForeignKey("Backlog_ID")]
        public Backlog Backlog { get; set; }
        public ICollection<Task> StoryTasks { get; set; }
    }
}
