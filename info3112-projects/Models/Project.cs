﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace info3112_projects.Models
{
    public class Project
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Project_ID { get; set; }
        public int Point_Hour_Ratio { get; set; }
        public string Team_Name { get; set; }
        public string Product_Name { get; set; }
        public string Hyperlink { get; set; }
        public DateTime? Start_Date { get; set; }
        public int Velocity { get; set; }
    }
}
