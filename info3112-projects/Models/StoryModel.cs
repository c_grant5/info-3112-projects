﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace info3112_projects.Models
{
    public class StoryModel
    {
        private AppDbContext _db;
        public TaskModel tm;
        public StoryModel(AppDbContext ctx)
        {
            _db = ctx;
        }

        public List<Story> GetAllStories()
        {
            tm = new TaskModel(_db);
            List<Story> stories = _db.Stories.ToList();
            foreach(Story s in stories)
            {
                s.StoryTasks = tm.GetAllTasksFromStory(s.Story_ID);
            }

            return stories;
        }

        public Story GetStory(int id)
        {
            tm = new TaskModel(_db);
            Story story = _db.Stories.FirstOrDefault(s => s.Story_ID == id);
            story.StoryTasks = tm.GetAllTasksFromStory(story.Story_ID);
            return story;
        }

        public List<Story> GetAllStoriesFromSprint(int id)
        {
            tm = new TaskModel(_db);
            List<Story> stories = _db.Stories.Where(story => story.Sprint_ID == id).ToList();
            foreach (Story s in stories)
            {
                s.StoryTasks = tm.GetAllTasksFromStory(s.Story_ID);
            }
            return stories;
        }

        public void UpdateStory(Story story)
        {
            try
            {
                using (_db)
                {
                    using (var _trans = _db.Database.BeginTransaction())
                    {
                        try
                        {
                            _db.Stories.Update(story);
                            _db.SaveChanges();
                            _trans.Commit();
                        }
                        catch(Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            _trans.Rollback();
                        }
                    }
                }
                    
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void ActualHours(int hours, int id)
        {
            _db.Stories.FirstOrDefault(s => s.Story_ID == id).Actual = hours;
            _db.SaveChanges();
        }

        public void AddStory(Story story)
        {
            try
            {
                using (_db)
                {
                    using (var _trans = _db.Database.BeginTransaction())
                    {
                        try
                        {
                            _db.Stories.Add(story);
                            _db.SaveChanges();
                            _trans.Commit();
                        }
                        catch(Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            _trans.Rollback();
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
