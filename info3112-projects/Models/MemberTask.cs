﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace info3112_projects.Models
{
    public class MemberTask
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Member_Task_ID { get; set; }
        public int Story_ID { get; set; }
        public double Hours_Worked { get; set; }
        [ForeignKey("Story_ID")]
        public Story Story { get; set; }
    }
}
