﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace info3112_projects.Models
{
    public class TeamMemberModel
    {
        private AppDbContext _db;
        public TeamMemberModel(AppDbContext ctx)
        {
            _db = ctx;
        }

        public TeamMember[] getAllMembers()
        {
            return _db.Set<TeamMember>().ToArray();
        }
    }
}
