﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace info3112_projects.Models
{
    public class TaskModel
    {
        private AppDbContext _db;
        public TaskModel(AppDbContext ctx)
        {
            _db = ctx;
        }

        public List<Models.Task> GetAllTasks()
        {
            return _db.Tasks.ToList();
        }

        public List<Models.Task> GetAllTasksFromStory(int id)
        {
            return _db.Tasks.Where(task => task.Story_ID == id).ToList();
        }

        public void AddTask(Task task)
        {
            try
            {
                using (_db)
                {
                    using (var _trans = _db.Database.BeginTransaction())
                    {
                        try
                        {
                            _db.Tasks.Add(task);
                            _db.SaveChanges();
                            _trans.Commit();
                        }
                        catch(Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            _trans.Rollback(); 
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
