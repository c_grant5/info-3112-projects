﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace info3112_projects.Models
{
    public class MemberTaskModel
    {
        private AppDbContext _db;
        public MemberTaskModel(AppDbContext ctx)
        {
            _db = ctx;
        }

        public List<MemberTask> GetAll()
        {
            return _db.MemberTasks.ToList();
        }

        public void AddMemberTask(MemberTask memberTask)
        {
            try
            {
                using (_db)
                {
                    using (var _trans = _db.Database.BeginTransaction())
                    {
                        try
                        {
                            _db.MemberTasks.Add(memberTask);
                            _db.SaveChanges();
                            _trans.Commit();
                        }
                        catch(Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            _trans.Rollback();
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
